﻿using System;
using System.Collections.Generic;

namespace ExtractEachKth
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] s = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

            int[] n = extractEachKth(s, 3);

            for (int i = 0; i < n.Length; i++)
            {
                System.Console.Write(n[i] + " ");
            }
        }
        static int[] extractEachKth(int[] inputArray, int k) {
            
            if(inputArray.Length < k){
                return inputArray;
            }
            
            List<int> listToReturn = new List<int>();
            int count = 1;
            for(int i = 0; i < inputArray.Length; i++){
                if(count == k){
                    count = 1;
                    continue;
                }
                listToReturn.Add(inputArray[i]);
                count++;
            }
            
            return listToReturn.ToArray();
        }


    }
}
